"""Tests on the PkgVersionMgr class."""

import os
import pytest
from pkg_version_mgr import PkgVersionMgr
import pkg_version_mgr as pv
import tests.mods.version as version_mod


class CustomThing(object):
    def __init__(self):
        self.called = False
        self.version = None

    def custom_func(self, version):
        self.version = version
        self.called = True


def test_basic():
    mgr = PkgVersionMgr(major=5, minor=1, micro=0, pipeline_id_name="", commit_tag_name="")

    ver_string = mgr.version()
    assert ver_string == "5.1.0"


def test_major_minor_suffix():
    mgr = PkgVersionMgr(major=5, minor=1, micro=None)

    ver_string = mgr.version()
    assert ver_string == "5.1"


def test_major_minor_micro():
    mgr = PkgVersionMgr(major=5, minor=1, micro=1, suffix=None)

    ver_string = mgr.version()
    assert ver_string == "5.1.1"


@pytest.mark.usefixtures('mgr_simple')
def test_version_file_bash(mgr_simple, tmp_path):
    target = str(tmp_path / 'VERSION')
    mgr_simple.version(target_file=target, target_fmt=mgr_simple.FMT_BASH)

    with open(target) as fp:
        content = fp.read()
        assert content == 'VERSION=5.1.0'


@pytest.mark.usefixtures('mgr_simple', 'tmp_path')
def test_version_file_str(mgr_simple, tmp_path):
    target = str(tmp_path / 'VERSION')
    mgr_simple.version(target_file=target, target_fmt=mgr_simple.FMT_STR)

    with open(target) as fp:
        content = fp.read()
        assert content == '5.1.0'


@pytest.mark.usefixtures('mgr_simple', 'this_dir', 'reset_version')
def test_version_module(mgr_simple, this_dir):
    mgr_simple.version(target_module=version_mod)

    with open(os.path.join(this_dir, 'mods', 'version.py')) as fp:
        assert '5.1.0' in fp.read()


@pytest.mark.usefixtures('mgr_simple', 'this_dir', 'reset_version')
def test_version_module_invalid_type(mgr_simple, this_dir):
    from pkg_version_mgr import InvalidParameterError

    with pytest.raises(InvalidParameterError):
        mgr_simple.version(target_module="invalid")


@pytest.mark.usefixtures('mgr_simple')
def test_custom_func(mgr_simple):
    custom = CustomThing()

    ver_string = mgr_simple.version(custom_func=custom.custom_func)

    assert custom.called
    assert custom.version == ver_string


@pytest.mark.usefixtures('mgr_simple', 'tmp_path')
def test_gen_all_outputs(mgr_simple, tmp_path):
    bash_target = str(tmp_path / 'VERSION')
    custom = CustomThing()

    ver_str = mgr_simple.version(target_file=bash_target,
                                 target_module=version_mod,
                                 target_fmt=mgr_simple.FMT_BASH,
                                 custom_func=custom.custom_func)

    with open(bash_target) as fp:
        assert '5.1.0' in fp.read()

    with open(version_mod.__file__) as fp:
        assert '__version__ = "5.1.0"' in fp.read()

    assert custom.version == ver_str


def test_ci_commit_tag(build_suffix, ci_pipeline_id):
    mgr = PkgVersionMgr(major=5, minor=1, micro=0)

    # Since the PKG_VER_SUFFIX_NUM is not defined, it should default to the pipeline ID
    assert mgr.version() == "5.1.0.{}{}".format(build_suffix, ci_pipeline_id)


def test_ci_pipeline_id_beta(ci_pipeline_id):
    mgr = PkgVersionMgr(major=5, minor=1, micro=0, suffix='b')

    assert mgr.version() == '5.1.0.b1234'
