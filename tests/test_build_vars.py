"""Tests on the PkgVersionMgr class."""

import os
import pytest
from pkg_version_mgr import PkgVersionMgr, SUFFIX, SUFFIX_NUM, MAJOR, MINOR, MICRO, PIPELINE_ID_GL


def setup_from_map(data):
    """

    :param data:
    :type data: dict
    :return:
    """
    for key, value in data.items():
        os.environ[key] = value


def teardown_from_map(data):
    """

    :param data:
    :type data: dict 
    :return: 
    """
    for key in data:
        del os.environ[key]


@pytest.fixture(name='full_dev')
def declare_full_dev():
    data = {
        SUFFIX: 'dev',
        SUFFIX_NUM: '1234',
        MAJOR: '3',
        MINOR: '2',
        MICRO: '0',
    }
    setup_from_map(data)
    yield
    teardown_from_map(data)

# ----------------------------------------------


def test_default():
    mgr = PkgVersionMgr(major=1, minor=2, micro=3)

    ver_string = mgr.version()
    assert ver_string == "1.2.3"


@pytest.mark.usefixtures('ci_pipeline_id')
def test_declare_pipeline_id(ci_pipeline_id):
    assert os.environ['CI_PIPELINE_ID'] == '1234'
    assert ci_pipeline_id == '1234'


@pytest.mark.usefixtures('ci_pipeline_id')
def test_default_with_pipeline_id(ci_pipeline_id):
    mgr = PkgVersionMgr(major=1, minor=2, micro=3, suffix="a", suffix_num=None)

    assert mgr.version() == '1.2.3.a{}'.format(ci_pipeline_id)


@pytest.mark.usefixtures('ci_pipeline_id', 'build_suffix')
def test_default_with_pipeline_and_suffix(ci_pipeline_id, build_suffix):
    mgr = PkgVersionMgr(major=1, minor=2, micro=3, suffix_num=PIPELINE_ID_GL)

    assert mgr.version() == '1.2.3.{}{}'.format(build_suffix, ci_pipeline_id)


@pytest.mark.usefixtures('ci_pipeline_id', 'build_suffix', 'build_suffix_num')
def test_default_with_pipeline_suffix_and_num(ci_pipeline_id, build_suffix, build_suffix_num):
    mgr = PkgVersionMgr(major=1, minor=2, micro=3, suffix_num=PIPELINE_ID_GL)

    assert mgr.version() == '1.2.3.{}{}'.format(build_suffix, ci_pipeline_id)


def test_default_without_pipeline():
    mgr = PkgVersionMgr(major=1, minor=2, micro=3)

    assert mgr.version() == '1.2.3'


@pytest.mark.usefixtures('ci_pipeline_id')
def test_with_static_suffix(ci_pipeline_id):
    mgr = PkgVersionMgr(major=1, minor=2, micro=3, suffix='xxx', suffix_num=PIPELINE_ID_GL)

    assert mgr.version() == '1.2.3.xxx{}'.format(ci_pipeline_id)


def test_no_pipeline_static_suffix():
    mgr = PkgVersionMgr(major=1, minor=2, micro=3, suffix='xxx', suffix_num=7)

    assert mgr.version() == '1.2.3.xxx7'


@pytest.mark.usefixtures('build_suffix_num')
def test_default_no_pipeline_static_suffix_var_num(build_suffix_num):
    mgr = PkgVersionMgr(major=1, minor=2, micro=3, suffix='xxx')

    assert mgr.version() == '1.2.3.xxx{}'.format(build_suffix_num)


@pytest.mark.usefixtures('ci_pipeline_id', 'build_suffix', 'build_suffix_num', 'ci_commit_tag')
def test_with_tag(ci_pipeline_id, build_suffix, build_suffix_num, ci_commit_tag):
    mgr = PkgVersionMgr(major=1, minor=2, micro=3)

    assert mgr.version() == ci_commit_tag


def test_with_major(major):
    mgr = PkgVersionMgr(major=MAJOR, minor=2, micro=3)

    assert mgr.version() == '{}.{}.{}'.format(major, 2, 3)


def test_with_major_minor(major, minor):
    mgr = PkgVersionMgr(major=MAJOR, minor=MINOR, micro=3)

    assert mgr.version() == '{}.{}.{}'.format(major, minor, 3)


def test_with_major_minor_micro(major, minor, micro):
    mgr = PkgVersionMgr(major=MAJOR, minor=MINOR, micro=MICRO)

    assert mgr.version() == '{}.{}.{}'.format(major, minor, micro)


@pytest.mark.usefixtures('full_dev')
def test_full(ci_pipeline_id):
    mgr = PkgVersionMgr(major=MAJOR, minor=MINOR, micro=MICRO, suffix=SUFFIX, suffix_num=SUFFIX_NUM)

    assert mgr.version() == '3.2.0.dev{}'.format(ci_pipeline_id)


@pytest.mark.usefixtures('full_dev')
def test_full_release(ci_pipeline_id):
    mgr = PkgVersionMgr(major=MAJOR, minor=MINOR, micro=MICRO, suffix=None, suffix_num=SUFFIX_NUM)

    assert mgr.version() == '3.2.0.{}'.format(ci_pipeline_id)


@pytest.mark.usefixtures('full_dev')
def test_full_tag(ci_pipeline_id, ci_commit_tag):
    mgr = PkgVersionMgr(major=MAJOR, minor=MINOR, micro=MICRO, suffix=None, suffix_num=SUFFIX_NUM)

    assert mgr.version() == ci_commit_tag


def test_all_static():
    mgr = PkgVersionMgr(major=4, minor=3, micro=2, suffix='abc', suffix_num='6')

    assert mgr.version() == '4.3.2.abc6'
