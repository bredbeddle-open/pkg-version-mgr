"""Fixtures, etc for PyTest."""
from __future__ import print_function
import sys
import os
import pytest
import pkg_version_mgr as pv
# from pkg_version_mgr import PkgVersionMgr, SUFFIX, SUFFIX_NUM, MAJOR, MINOR, MICRO, PIPELINE_ID_GL


@pytest.fixture(scope='session')
def this_dir():
    """
    Returns this test directory full path.

    :return:
    """
    return _this_dir()


def _this_dir():
    """

    :return:
    """
    pwd = os.path.dirname(os.path.abspath(sys.modules[__name__].__file__))
    return pwd


@pytest.fixture(name='reset_version')
def generate_version_file(request):
    """
    This will generate/reset the version.py file for tests.

    :return:
    """
    def fin():
        _generate_version_module()

    request.addfinalizer(fin)

    _generate_version_module()


def _generate_version_module():
    filename = os.path.join(_this_dir(), 'mods', 'version.py')

    with open(filename, 'w+') as fp:
        print('__version__ = "1.2.3.a1"', file=fp)


@pytest.fixture(name='ci_pipeline_id')
def declare_pipeline_id(key='CI_PIPELINE_ID'):
    os.environ[key] = '1234'
    yield os.environ[key]
    del os.environ[key]


@pytest.fixture(name='build_suffix')
def declare_build_suffix(key=pv.SUFFIX):
    os.environ[key] = 'dev'
    yield os.environ[key]
    del os.environ[key]


@pytest.fixture(name='build_suffix_num')
def declare_build_suffix_num(key=pv.SUFFIX_NUM):
    os.environ[key] = '3'
    yield os.environ[key]
    del os.environ[key]


@pytest.fixture(name='ci_commit_tag')
def declare_tag(key='CI_COMMIT_TAG'):
    os.environ[key] = '8.0.8'
    yield os.environ[key]
    del os.environ[key]


@pytest.fixture(name='major')
def declare_major(key=pv.MAJOR):
    os.environ[key] = '8'
    yield os.environ[key]
    del os.environ[key]


@pytest.fixture(name='minor')
def declare_minor(key=pv.MINOR):
    os.environ[key] = '0'
    yield os.environ[key]
    del os.environ[key]


@pytest.fixture(name='micro')
def declare_micro(key=pv.MICRO):
    os.environ[key] = '9'
    yield os.environ[key]
    del os.environ[key]


@pytest.fixture(scope='session', name='mgr_simple')
def get_simple_mgr():
    return pv.PkgVersionMgr(major=5, minor=1, micro=0, pipeline_id_name="", commit_tag_name="")


@pytest.fixture(name='mgr_full')
def get_full_mgr():
    return pv.PkgVersionMgr(major=5, minor=1, micro=0, suffix=pv.SUFFIX, suffix_num=pv.SUFFIX_NUM)
