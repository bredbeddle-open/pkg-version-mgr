# Overview

[![pipeline status](https://gitlab.com/bredbeddle-open/pkg-version-mgr/badges/master/pipeline.svg)](https://gitlab.com/bredbeddle-open/pkg-version-mgr/-/commits/master)
 [![coverage report](https://gitlab.com/bredbeddle-open/pkg-version-mgr/badges/master/coverage.svg)](https://gitlab.com/bredbeddle-open/pkg-version-mgr/-/commits/master)

Helper library to streamline generating Python package versions with an eye on building in a pipeline.

# Overview

This library evolved from the need to generate version strings from Gitlab pipeline ID with
changes for the various release stages like, dev build, alpha and release tags.

When creating new Python projects we had some boiler-plate code that we pasted into the new "setup.py".
Now we create the new setup module like this:

```python

import os
import sys
from setuptools import setup

sys.path.insert(0, os.path.join(os.path.dirname(__file__), 'src'))

import pkg_version_mgr.version as version  # noqa
import pkg_version_mgr as pv  # noqa

version_mgr = pv.PkgVersionMgr(major=pv.MAJOR, minor=pv.MINOR, micro=pv.MICRO)

# The setup.cfg contains all common option/metadata definitions
setup(
   version=version_mgr.version(target_module=version),
)
```

The above will generate the version string from environment variables defined in the `.gitlab-ci.yml`
and [Gitlab project CI/CD variables](https://docs.gitlab.com/ee/ci/variables/README.html).

It will also automatically update the "version.py" module's "__version__" value before doing
the build.

See:  [setuptools documentation](https://setuptools.readthedocs.io/en/latest/)

The :class:`PkgVersionMgr <pkg_version_mgr.PkgVersionMgr>` class provides flexibility for
handling the version calculation with optional update of version files. It, by default, understands
and uses the Gitlab CI environment variables.

## Installation

```bash
pip install pkg-version-mgr
```

The package has no (real) external dependencies and supports Python 2.7 and 3.7+.

New Python projects should add "pkg-version-mgr" to their "requirements.txt".
