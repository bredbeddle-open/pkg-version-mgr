#!/usr/bin/env python
"""Setup for building.

This is both for building the package an as an example of use.

"""
import os
import sys
from setuptools import setup

sys.path.insert(0, os.path.join(os.path.dirname(__file__), 'src'))

import pkg_version_mgr.version as version  # noqa
import pkg_version_mgr as pv  # noqa

# Configure the version to use variables for major, minor, micro and suffix_num.
# These are defined in the Gitlab project CI/CD variables.
version_mgr = pv.PkgVersionMgr(major=pv.MAJOR, minor=pv.MINOR, micro=pv.MICRO)

# Or you could hard-code as...
# version_mgr = PkgVersionMgr(major=0, minor=1, micro=0, suffix='dev')

# The setup.cfg contains all common option values
setup(
    version=version_mgr.version(target_module=version),
    install_requires=['future'],
)
