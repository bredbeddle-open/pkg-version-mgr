Overview
========

.. note::

   Docs are still work-in-progress. Be gentle.

When creating new Python projects we had some boiler-plate code that we pasted into the new "setup.py".
Now we create the new setup module like this:

.. code-block:: python
   :caption: setup.py

   import os
   import sys
   from setuptools import setup

   sys.path.insert(0, os.path.join(os.path.dirname(__file__), 'src'))

   import pkg_version_mgr.version as version  # noqa
   import pkg_version_mgr as pv  # noqa

   version_mgr = pv.PkgVersionMgr(major=pv.MAJOR, minor=pv.MINOR, micro=pv.MICRO)

   # The setup.cfg contains all common option/metadata definitions
   setup(
       version=version_mgr.version(target_module=version),
   )

The above will generate the version string from environment variables defined in the `.gitlab-ci.yml`
and `Gitlab project CI/CD variables <https://docs.gitlab.com/ee/ci/variables/README.html>`_.

It will also automatically update the "version.py" module's `__version__ <https://www.python.org/dev/peps/pep-0396/>`_
value before doing the build.

The :class:`PkgVersionMgr <pkg_version_mgr.PkgVersionMgr>` class provides flexibility for
handling the version calculation with optional update of version files. It, by default, understands
and uses the Gitlab CI environment variables.

* CI_PIPELINE_ID: Gitlab CI pipeline ID
* CI_COMMIT_TAG: Gitlab CI commit tag

.. seealso::

   `Setuptools Documentation <https://setuptools.readthedocs.io/en/latest/>`_
      Standard Python setup.py documentation

   :ref:`variables`

Usage
-----

.. note::

   Most of the examples and use-cases presume building within Gitlab pipelines. They equally
   apply to building locally and in other pipelines such as GitHub.

Simple Method
~~~~~~~~~~~~~

The easiest method for specifying the version components is to hard-code it within the "setup.py". However, this
requires updating the "setup.py" for each version bump.

This presupposes:

#. Your package name is "your_package"
#. There is a module "your_package/version.py" with `__version__` defined
#. On calculating the version the "version.py" module will be updated before packaging
#. Building locally (i.e., "python setup.py bdist_wheel")

The following will set the version to "1.0.0.dev1"

.. code-block:: python
   :caption: setup.py

   import pkg_verion_mgr as pv
   import your_package.version as version

   version_mgr = pv.PkgVersionMgr(major=1, minor=0, micro=0, suffix='dev', suffix_num=1)

   setuptools.setup(
       # Provides the version to setup and updates the "version.py" module
       version=version_mgr.version(target_module=version),
   )

.. code-block:: python
   :caption: version.py

   """Example project version file that can be auto-updated during building."""

   __version__ = "0.1.0.dev1"

Basic Pipeline Build
~~~~~~~~~~~~~~~~~~~~

Rather than hard-code version components in the "setup.py", they can instead be injected from
variables set in the pipeline build scripts.

To build a package in a Gitlab pipeline, we will want to externalize some - or all - aspects
of the version components for dev, alpha or release builds.

.. code-block:: python
   :caption: setup.py

   import pkg_verion_mgr as pv
   import your_package.version as version

   version_mgr = pv.PkgVersionMgr(major=pv.MAJOR, minor=pv.MINOR, micro=pv.MICRO)

   setuptools.setup(
       # Provides the version to setup and updates the "version.py" module
       version=version_mgr.version(target_module=version),
   )

The build will generate the version string from variables set either within the `.gitlab-ci.yml` or the
Gitlab project's `CI/CD variables <https://docs.gitlab.com/ee/ci/variables/README.html>`_.

In the ".gitlab-ci.yml" we add a stage to build:

.. code-block:: yaml
   :caption: .gitlab-ci.yml

   stages:
     - syntax
     - test
     - deploy

   # Other stages...

   build_package:
      stage: deploy
      image: python:3.9
      variables:
         # These next 3 could also be defined in the CI/CD variables for the project.
         PKG_VER_MAJOR: 2
         PKG_VER_MINOR: 0
         PKG_VER_MICRO: 3
         PKG_VER_SUFFIX: 'dev'
      before_script:
         - pip install -r requirements.txt
         - pip install setuptools wheel twine
      script:
         - python setup.py bdist_wheel --universal

When the pipeline passes the version will look like: "2.0.3.dev77420".

Once development testing is complete and we start building an alpha release, the "PKG_VER_SUFFIX" is changed
to "a" (or similar suffix) in the `.gitlab-ci.yml` (or CI/CD variables).

Alternatively, a tag *could* be applied to the branch with something like "RC1". This would produce a version
string "2.0.3.RC1".

To build a release version the "PKG_VER_SUFFIX" can be set to none to generate a version string
"2.0.3.<pipeline_id>".

For tag pipelines the version will be derived from the value of "CI_COMMIT_TAG" alone. For example, if the
tag "2.0.3" is created, the tag pipeline will then
have `CI_COMMIT_TAG <https://docs.gitlab.com/ee/ci/variables/predefined_variables.html>`_ defined. The tag **must**
be a valid version string.
