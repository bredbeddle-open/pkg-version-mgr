pkg\_version\_mgr package
=========================

Submodules
----------

pkg\_version\_mgr.version module
--------------------------------

.. automodule:: pkg_version_mgr.version
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pkg_version_mgr
   :members:
   :undoc-members:
   :show-inheritance:
