.. _variables:

PkgVersionMgr Variables
=======================

To support flexible Gitlab/GitHub pipelines, all the component parts of the version
generation can derive their values from environment variables.

.. note::

    Most of my experience with build pipelines is on Gitlab. I welcome suggestions from
    experienced GitHub, or other CI environments.

To generate the version string *only* from CI environment variables:

.. code-block:: python
   :caption: setup.py

   import pkg_version_mgr as pv

   # The constructor already defaults `suffix` and `suffix_num` to variables.
   mgr = pv.PkgVersionMgr(major=pv.MAJOR, minor=pv.MINOR, micro=pv.MICRO)

Version Components
------------------

Each of the component parts of the version may either be hard-code or generated from environment
variables.

The general structure is: MAJOR.MINOR.MICRO.SUFFIX+SUFFIX_NUM

.. code-block:: text

        3.0.1.dev12345
        | | |  |   |- SUFFIX_NUM
        | | |  |- SUFFIX
        | | |- MICRO
        | |- MINOR
        |- MAJOR


.. list-table:: Environment Variables
  :header-rows: 1
  :widths: 40 90 100
  :align: left

  * - Parameter
    - Variable
    - Environment Variable
  * - Major
    - :py:data:`pkg_version_mgr.MAJOR`
    - PKG_VER_MAJOR
  * - Minor
    - :py:data:`pkg_version_mgr.MINOR`
    - PKG_VER_MINOR
  * - Micro
    - :py:data:`pkg_version_mgr.MICRO`
    - PKG_VER_MICRO
  * - Suffix
    - :py:data:`pkg_version_mgr.SUFFIX`
    - PKG_VER_SUFFIX
  * - Suffix Num
    - :py:data:`pkg_version_mgr.SUFFIX_NUM`
    - PKG_VER_SUFFIX_NUM

Variables
~~~~~~~~~

The use of environment variable is totally optionally, but helps to create pipelines that
build Python packages with fully-externalized version string rendering.

.. note::

    For Gitlab pipelines:

    #. The values for variables such as PKG_VER_MAJOR can be added to the project's CI/CD variables
    #. When a tag is added (like to a release branch), the CI_COMMIT_TAG is used instead of other variables

.. seealso:: https://docs.gitlab.com/ee/ci/variables/README.html

The following table shows the results for the composite of `suffix` and `suffix_num`.

Given:

* The major/minor/micro of "2.2.3":
* CI_PIPELINE_ID="4321"

.. list-table:: Value for SUFFIX + SUFFIX_NUM
  :header-rows: 1
  :widths: 120 120 100
  :align: left

  * - suffix Parameter
    - suffix_num Parameter
    - Result String
  * - None
    - None
    - "2.2.3"
  * - "dev"
    - "4242"
    - "2.2.3.dev4242"
  * -
       :py:data:`pkg_version_mgr.SUFFIX`

       PKG_VER_SUFFIX="a"
    -
       :py:data:`pv.SUFFIX_NUM <pkg_version_mgr.SUFFIX_NUM>`

       PKG_VER_SUFFIX_NUM="4242"
    - "2.2.3.a4242"
  * - None
    -
       :py:data:`pv.SUFFIX_NUM <pkg_version_mgr.SUFFIX_NUM>`

       PKG_VER_SUFFIX_NUM=$CI_PIPELINE_ID
    - "2.2.3.4321"
  * - "CI_PIPELINE_ID"
    - None
    - "2.2.3.4321"
  * -
       "BUILD_YEAR"

       BUILD_YEAR="2021"
    -
       "BUILD_MONTH"

       BUILD_MONTH="02"
    - "2.2.3.202102"
  * -
       :py:data:`pkg_version_mgr.SUFFIX`

       PKG_VER_SUFFIX not defined
    -
       :py:data:`pv.SUFFIX_NUM <pkg_version_mgr.SUFFIX_NUM>`

       PKG_VER_SUFFIX_NUM not defined
    - "2.2.3"

Major/Minor/Micro
~~~~~~~~~~~~~~~~~

These may be either statically defined in code, or reference external environment variables:


For situations where you will not be building a dev release, then an alpha followed by the release build,
you _could_ use a slight "trick".

.. code-block:: python
   :caption: setup.py

   mgr = pv.PkgVersionMgr(major=1, minor=0, micro='CI_PIPELINE_ID', suffix=None, suffix_num=None)

This will result in unique version strings with the CI pipeline ID used for the micro (patch) version.

Suffix/Suffix_num
~~~~~~~~~~~~~~~~~

The 4th segment of the version string is optional. For release versions it is generally omitted, or is set
to the numeric patch release.

For development/pre-release builds it is often a composite value of: <pre_tag> + <pipeline_id>.

PkgVersionMgr concatenates the value for `suffix` + `suffix_num`.

If the parameter :py:meth:`suffix <pkg_version_mgr.PkgVersionMgr>` is passed None, no value is generated
for suffix. If set to :py:data:`pkg_version_mgr.SUFFIX`, the value of
the PKG_VER_SUFFIX environment variable is joined with the value for `suffix_num`.


Generating Files
----------------

While generating output files is optional, generally we will update the "version.py"
module as part of the CI build.

.. list-table:: Supported Output Formats
  :header-rows: 1
  :widths: 35 100
  :align: left

  * - Target
    - Description
  * - target_file
    - Generate file of format specified with ``target_fmt``
  * - target_module
    - Generate Python module. Same as ``target_file=<path_to_module>`` with ``target_fmt=FMT_PYTHON``
  * - custom_func
    - Call supplied custom function with the version string as the only parameter

target_module
~~~~~~~~~~~~~

The call to :class:`version() <pkg_version_mgr.PkgVersionMgr.version>` will update/generate file(s)
with the version string.

For local builds during testing it's not always desirable to have the "version.py" module touched. Set
the environment variable :py:const:`PKG_VER_SKIP_TARGET_MODULE <pkg_version_mgr.SKIP_UPDATE_VERSION>`
(to any value) to skip writing to the "version.py".

.. code-block:: bash
   :caption: Local build example

   PKG_VER_SKIP_TARGET_MODULE=1 python setup.py bdist_wheel

A project structure will look similar to:

.. code-block:: text
   :caption: Source tree

   project_root
      your_package
         __init__.py
         version.py
         main.py

.. code-block:: python
   :caption: setup.py

   import your_project.version as version
   setup(
       version=version_mgr.version(target_module=version),
   )

target_file
~~~~~~~~~~~

Some build scenarios require writing the version into a custom, external file.
``target_file`` parameter of the :py:meth:`version <pkg_version_mgr.PkgVersionMgr.version>` accepts
the name of a file to write the version to in the format defined by ``target_fmt``. The file is
created or overwritten if it exists.

The `target_fmt` defines what format the file should be written as.

.. table::
   :align: left
   :widths: auto

   ===========  ========================================================
   target_fmt   Description
   ===========  ========================================================
   FMT_STR      Only the version string is written to the file
   FMT_PYTHON   Generates the file as a Python module (i.e., version.py)
   FMT_BASH     Generates with VERSION="x.x.x.x" in shell format
   ===========  ========================================================

Default: FMT_PYTHON

.. code-block:: python
   :caption: setup.py

   filename = "./VERSION"

   setuptools.setup(
      # Provides the version to setup and updates the "version.py" module
      version=version_mgr.version(target_file=filename, target_fmt=version_mgr.FMT_STR),
   )

custom_func
~~~~~~~~~~~

This parameter is used to call a custom function to "write" the version to. It fits needs beyond
what is implemented: Generate or update YAML, JSON. Write to a DB or call some REST API. Etc.

The function should have the signature:

.. code-block:: python

   def foo(version: str)


Example of using ``custom_func``.

.. code-block:: python
   :caption: setup.py

   def custom_build_foo(version: str):
       print('The generated version: {}'.format(version))

   # ...
   setuptools.setup(
       # Provides the version to setup and updates the "version.py" module
       version=version_mgr.version(target_module=version, custom_func=custom_build_foo),
   )

While the use-cases for the custom function might be rare, it provides an easy hook for perform custom
options on building. In one project I updated the Sphinx content to have the build version.

Multiple Targets
~~~~~~~~~~~~~~~~

For the overachievers, it should be noted that all - or none - of the output targets can be used
together.

.. code-block:: python
   :caption: setup.py

   import pkg_version_mgr as pv

   def update_some_yaml_file(version: str):
      with open('some-file.yaml') as fp:
         data = yaml.safe_load(fp)

      data['some-key'] = version
      with open('some-file.yaml', 'w+') as fp:
         yaml.safe_dump(data, fp)

   filename = "./VERSION"

   setuptools.setup(
      version=version_mgr.version(target_module=version,
                                  target_file=filename, target_fmt=version_mgr.FMT_BASH,
                                  custom_func=update_some_yaml_file)
   )